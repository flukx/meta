#!/bin/bash
set -e
cd $(dirname "$0")/..

source ./config

echo "# init event repository"
mkdir repo
cd repo 
# git clone https://gitlab.com/trichter/leipzig.git master
git clone https://gitlab.com/flukx/reykjavik.git master
cd master
echo "using git user ${USER} with email ${USER}@${HOST} but it doesn't really matter"
git config user.email "${USER}"'@'"${HOST}"
git config user.name "${USER}"

echo "========================"
echo "# init builder"
cd ../..
git clone https://gitlab.com/flukx/builder.git
cd builder
yarn

echo "========================"
echo "# init scraper"
cd ..
git clone https://gitlab.com/flukx/scraper.git
cd scraper
yarn
